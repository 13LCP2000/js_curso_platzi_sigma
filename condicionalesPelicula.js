
//ahora se veran quienes pueden ver una pelicula y para esto veremos un ejercicio con javascript, basado en condicionales...
const namePelicu = 'nombre de la pelicula2';
const edadMinima = 15;

//variables de para el  sujeto1
const nameLeo = 'Leo';
const edadLeo = 23;

//variables para el sujeto2
const nameTom = 'Tom';
const edadTom = 13;

//funciones para la pelicula...
function puedeVerPelicula(nombre, edad){
	if (edad >= edadMinima){
		console.log(`${nombre} puede pasar a ver ${namePelicu}`);
	}
	else{
		console.log(`${nombre} no puede pasar a ver ${namePelicu}.
			Tiene ${edad} años y necesita tener ${edadMinima}`);
	}
}

//probando las funciones...
puedeVerPelicula(nameLeo, edadLeo); //puede ver la pelicula.
puedeVerPelicula(nameTom, edadTom); //Nop puede ver la pelicula.
